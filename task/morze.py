def code_morze(value):
    morse_code_dict = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.',
        'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---',
        'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---',
        'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-',
        'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--',
        'Z': '--..',
        '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....',
        '6': '-....', '7': '--...', '8': '---..', '9': '----.', '0': '-----',
        ',': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'
    }

    def encode_char(char):
        if char.upper() in morse_code_dict:
            return morse_code_dict[char.upper()]
        else:
            return ''

    morse_code = ''
    previous_char_was_space = False

    for char in value:
        if char == ' ':
            if not previous_char_was_space:
                morse_code += ' '
                previous_char_was_space = True
        else:
            encoded_char = encode_char(char)
            if encoded_char:
                morse_code += encoded_char + ' '
                previous_char_was_space = False

    return morse_code.strip()  

